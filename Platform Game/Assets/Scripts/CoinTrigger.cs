﻿using UnityEngine;
using System;

public class CoinTrigger : MonoBehaviour
{
    public GameController gameController;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.tag.Equals("Coin") == false)
            return;
        if (collision.tag.Equals("Player") && !collision.isTrigger)
            gameController.CollectCoin(gameObject);
    }
}
