﻿using UnityEngine;

[RequireComponent(typeof(GameController))]
public class PlayerCollision : MonoBehaviour
{
    public GameController gameController;
    bool isTriggered = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.tag.Equals("Player") == false)
            return;
        if (collision.tag.Equals("Grass") || collision.tag.Equals("Ramp"))
            isTriggered = true;
        else if (collision.tag.Equals("LoseField"))
            gameController.ReloadLevel();
        else if (collision.tag.Equals("Enemy"))
            gameController.ReloadLevel();
        else if (collision.tag.Equals("Chest"))
        {
            collision.gameObject.GetComponent<Animator>().enabled = true;
            gameController.LoadNextLevel();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.tag.Equals("Player") == false)
            return;
        if (collision.tag.Equals("Grass") || collision.tag.Equals("Ramp"))
            isTriggered = false;
    }

    public bool IsOnGround()
    {
        return isTriggered;
    }
}
