﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TextCollisions : MonoBehaviour
{ 
    new public Rigidbody2D rigidbody2D;
    public int IsJumpTriggered { get; }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.name.Equals("TmpJump") && collision.tag.Equals("Player"))
        {
            rigidbody2D.gravityScale = -1f;
            Destroy(gameObject, 3f);
        }
        if(gameObject.name.Equals("TmpTooHigh") && collision.tag.Equals("Player"))
        {
        }
    }
}
