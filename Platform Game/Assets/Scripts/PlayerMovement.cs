﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    #region Fields
    public Rigidbody2D Rigidbody2D;
    public float MaxMovementSpeed = 20f;
    [Range(1f, 100f)]
    public float MaxJumpForce = 12f;
    [Range(0.0001f, 1f)]
    public float Interness = 1f;

    PlayerCollision playerCollision;
    Vector2 playerVelocity;
    float movementForce;
    float jumpForce;
    bool blockJump = false;

    #endregion
    private void Awake()
    {
        playerCollision = GetComponent<PlayerCollision>();
    }
    private void Update()
    {
        movementForce = Input.GetAxisRaw("Horizontal") * MaxMovementSpeed;
        jumpForce = Input.GetAxisRaw("Vertical") * MaxJumpForce;
    }

    void FixedUpdate()
    {
        playerVelocity = Rigidbody2D.velocity;
        if(playerCollision.IsOnGround())
        {
            if (blockJump == false)
            {
                if(jumpForce > 0f)
                {
                    Rigidbody2D.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                    blockJump = true;
                    Invoke("ReleaseJump", 0.5f);
                }
            }

            if ((playerVelocity.x < -1f && movementForce > 0f) || (playerVelocity.x > 1f && movementForce < 0f))
                Rigidbody2D.velocity = new Vector2(playerVelocity.x * Interness, playerVelocity.y);
            
        }
        if(Mathf.Abs(Rigidbody2D.velocity.x) < 5f)
            Rigidbody2D.AddForce(Vector2.right * movementForce * Time.fixedDeltaTime, ForceMode2D.Impulse);
    }

    void ReleaseJump()
    {
        blockJump = false;
    }
}
