﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject coinParticle;
    public TextMeshProUGUI tmpCoins;
    int scores = 0;
    readonly string scoreString = "Coins: ";

    void Start()
    {
        tmpCoins.text = scoreString + "0";
    }

    internal void CollectCoin(GameObject gameObject)
    {
        
        var particle = Instantiate(coinParticle, gameObject.transform.position, Quaternion.identity);
        Destroy(gameObject);
        Destroy(particle, 0.5f);
        scores++;
        tmpCoins.text = scoreString + scores.ToString();
    }

    internal void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    internal void LoadNextLevel()
    {
        Invoke("LoadNextScene", 1.2f);
    }

    private void LoadNextScene()
    {
        var nextLevelIndex = SceneManager.GetActiveScene().buildIndex + 1;
        try
        {
            var nextLevel = SceneManager.GetSceneByBuildIndex(nextLevelIndex);
            SceneManager.LoadScene(nextLevelIndex);
        }
        catch (System.Exception e)
        {
            //TODO add finish scene
            Debug.Log($"There is no Scene at {nextLevelIndex}" + e.Message);
            SceneManager.LoadScene(0);
        }
    }
}
